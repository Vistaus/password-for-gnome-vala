<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
  <id>org.emilien.Password.desktop</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0-or-later</project_license>
  <name>Password</name>
  <summary>Strong Password for Maximum Security</summary>
  <description>
    <p>
      Password is a password calculator and random generator for Gnome written in Gtk/Vala.
    </p>
    <p>
      This app calculates strong unique passwords for each alias and passphrase combination.
      No need to remember dozens of passwords any longer and no need for a password manager!
    </p>
    <p>
      The calculator can use MD5, SHA1, SHA256 and SHA512 hash algorithms in combination with HMAC.
    </p>
    <p>
      Compatible with Linux Phone using Phosh (PinePhone, Librem 5).
    </p>
  </description>
  <screenshots>
    <screenshot type="default">
      <image>https://gitlab.com/elescoute/password-for-gnome-vala/raw/master/data/appdata/screenshot01.png</image>
      <caption>Calculator page</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/elescoute/password-for-gnome-vala/raw/master/data/appdata/screenshot02.png</image>
      <caption>Calculator page</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/elescoute/password-for-gnome-vala/raw/master/data/appdata/screenshot03.png</image>
      <caption>Random generator page</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/elescoute/password-for-gnome-vala/raw/master/data/appdata/screenshot04.png</image>
      <caption>Random generator page</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/elescoute/password-for-gnome-vala/raw/master/data/appdata/screenshot05.png</image>
      <caption>Settings page</caption>
    </screenshot>
  </screenshots>
  <releases>
    <release version="1.2.6" date="2021-11-27">
      <description>
        <ul>
        <li>Add asynchronous internet requests (password integrity check) to avoid gui freeze.</li>
        </ul>
      </description>
    </release>
    <release version="1.2.5" date="2021-11-14">
      <description>
        <ul>
        <li>Fix bug if no internet connection.</li>
        </ul>
      </description>
    </release>
    <release version="1.2.4" date="2021-11-12">
      <description>
        <p>New tool to manually check password integrity.</p>
        <p>Fix some minor bugs.</p>
      </description>
    </release>
    <release version="1.2.3" date="2021-11-08">
      <description>
        <ul>
        <li>Add check password integrity (compare with HaveIBeenPwned database).</li>
        <li>New dark mode button (inspired from com.github.unrud.VideoDownloader).</li>
        <li>Fix some minor bugs.</li>
        </ul>
      </description>
    </release>
    <release version="1.2.2" date="2021-10-30">
      <description>
        <ul>
        <li>New icon.</li>
        </ul>
      </description>
    </release>
    <release version="1.2.1" date="2021-10-29">
      <description>
        <ul>
        <li>Gnome 41.</li>
        <li>Code cleaned.</li>
        <li>Alias can be hidden.</li>
        </ul>
      </description>
    </release>
    <release version="1.2.0" date="2021-06-15">
      <description>
        <ul>
        <li>UI improvements.</li>
        <li>Use libhandy.</li>
        </ul>
      </description>
    </release>
    <release version="1.1.2" date="2021-06-06">
      <description>
        <ul>
        <li>Icon resized.</li>
        </ul>
      </description>
    </release>
    <release version="1.1.1" date="2021-05-03">
      <description>
        <ul>
        <li>Gnome 40.</li>
        </ul>
      </description>
    </release>
    <release version="1.1.0" date="2021-03-22">
      <description>
        <ul>
        <li>Add the possibility to create settings profiles.</li>
        </ul>
      </description>
    </release>
    <release version="1.0.9" date="2021-02-28">
      <description>
        <ul>
        <li>Change location of config files.</li>
        </ul>
      </description>
    </release>
    <release version="1.0.8" date="2021-01-31">
      <description>
        <ul>
        <li>Fix display for phosh devices.</li>
        </ul>
      </description>
    </release>
    <release version="1.0.7" date="2021-01-30">
      <description>
        <ul>
        <li>Settings and key files are now encrypted if a lock code is set.</li>
        </ul>
      </description>
    </release>
    <release version="1.0.6" date="2021-01-22">
      <description>
        <ul>
        <li>Spanish translation by Óscar Fernández Díaz.</li>
        <li>Add colored buttons.</li>
        </ul>
      </description>
    </release>
    <release version="1.0.5" date="2020-12-29">
      <description>
        <ul>
        <li>Fix bug.</li>
        </ul>
      </description>
    </release>
    <release version="1.0.4" date="2020-12-29">
      <description>
        <ul>
        <li>Possibility to lock settings page with a confidential code.</li>
        <li>Dark theme.</li>
        </ul>
      </description>
    </release>
    <release version="1.0.3" date="2020-12-27">
      <description>
        <ul>
        <li>Add calculation of the password strength according to ANSSI method.</li>
        <li>For mobile phones only (with phosh): Includes a web view of Firefox Monitor to search your email address in public data breaches.</li>
        </ul>
      </description>
    </release>
    <release version="1.0.2" date="2020-12-22">
      <description>
        <ul>
        <li>Swedish translation by Åke Engelbrektson.</li>
        </ul>
      </description>
    </release>
    <release version="1.0.1" date="2020-12-19">
      <description>
        <ul>
        <li>Update to gnome 3.38.</li>
        <li>Add possibility to generate and calculate passwords with numbers only.</li>
        </ul>
      </description>
    </release>
    <release version="1.0" date="2020-04-28">
      <description>
        <ul>
        <li>Initial version.</li>
        </ul>
      </description>
    </release>
  </releases>
  <url type="homepage">https://gitlab.com/elescoute/password-for-gnome-vala</url>
  <update_contact>emilien.lescoute@lilo.org</update_contact>
  <launchable type="desktop-id">org.emilien.Password.desktop</launchable>
  <developer_name>Emilien Lescoute</developer_name>
  <project_group>GNOME</project_group>
  <translation type="gettext">password</translation>
  <content_rating type="oars-1.0" />
  <requires>
    <display_length compare="ge">360</display_length>
  </requires>
  <custom>
     <value key="Purism::form_factor">workstation</value>
     <value key="Purism::form_factor">mobile</value>
   </custom>
</component>

