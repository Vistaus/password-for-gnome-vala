int calculForce(int length, bool remLower, bool remUpper, bool remNum, bool remSpec, bool onlyPin){

    int force = 0;
    int N=0;
    if(!remLower) N += 26;
    if(!remUpper) N += 26;
    if(!remNum) N += 10;
    if(!remSpec) N += 3;
    if(onlyPin) N = 10;

    // N^(LENGTH)
    //double num2 = Math.pow(N,length);

    for (int i=0;i <=128; i++) {
        if(Math.pow(2,i) < Math.pow(N,length)) force=i;
    }

    return force;
}
