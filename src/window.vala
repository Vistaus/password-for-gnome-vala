/* window.vala
 *
 * Copyright 2020-2021 Emilien
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//using WebKit;


//VARIABLES GLOBALES
string randomPassword;
string pass;
string passwordCourt;
string dossierConfig;
bool quitterAppli;
bool attendreAvanDeFermer;
bool notificationEffacement;
bool phosh;
int largeur;
int chgt;
int champ;
string url;
string codeVerrouillage;
bool verrou;
string cle1;
string cle2;
string uuid;
int nbProfils;
string profilActuel;
List<string> listeProfils;
List<Gtk.ModelButton> boutonChargProfil;

namespace Password {
	[GtkTemplate (ui = "/org/emilien/Password/window.ui")]


	public class Window : Hdy.ApplicationWindow {

		//WIDGETS


		[GtkChild] public unowned Hdy.HeaderBar headerbar;
		[GtkChild] public unowned Gtk.Stack fenetre;
		[GtkChild] public unowned Gtk.Box fenetreCalculateur;
		[GtkChild] public unowned Gtk.Box fenetreGenerateur;
		[GtkChild] public unowned Gtk.Box fenetreReglages;
		//[GtkChild] public unowned Hdy.PreferencesPage fenetreReglages;
		[GtkChild] public unowned Gtk.Box fenetreDeverrouillage;
		[GtkChild] public unowned Gtk.Box fenetreVerrouillage;
		[GtkChild] public unowned Gtk.Box fenetrePwned;
		[GtkChild] public unowned Gtk.Revealer revealBarre;
		[GtkChild] public unowned Gtk.Revealer revealHaut;
		[GtkChild] public unowned Gtk.Revealer revealBas;
		[GtkChild] public unowned Gtk.Box barre;
		[GtkChild] public unowned Gtk.Image iconeBoutonNettoyer;
		[GtkChild] public unowned Gtk.Button boutonNettoyer;
		[GtkChild] public unowned Gtk.Revealer revealCopier;
		[GtkChild] public unowned Gtk.Button boutonCopier;
		[GtkChild] public unowned Gtk.Button boutonPwned;
		[GtkChild] public unowned Gtk.Button boutonPwned2;
		[GtkChild] public unowned Gtk.Switch switchMask;
		[GtkChild] public unowned Gtk.Image iconeSwitchMask;
		[GtkChild] public unowned Gtk.ModelButton boutonReset;
		[GtkChild] public unowned Gtk.ModelButton boutonPropos;
		[GtkChild] public unowned Gtk.ModelButton boutonQuitter;
		[GtkChild] public unowned Gtk.Image iconeBoutonMenuMode;
		[GtkChild] public unowned Gtk.Image iconeCalculateur;
		[GtkChild] public unowned Gtk.Image iconeGenerateur;
		[GtkChild] public unowned Gtk.Image iconeReglages;
		[GtkChild] public unowned Gtk.Image iconeMonitor;
		[GtkChild] public unowned Gtk.Image iconePwn;
		[GtkChild] public unowned Gtk.Image iconeBouclier;
		[GtkChild] public unowned Gtk.Label barre0;
		[GtkChild] public unowned Gtk.Label barre1;
        [GtkChild] public unowned Gtk.Label barre2;
		[GtkChild] public unowned Gtk.Label barre3;
		[GtkChild] public unowned Gtk.Label barre4;
		[GtkChild] public unowned Gtk.Label labelForce;
		[GtkChild] public unowned Gtk.ModelButton boutonModeCalculateur;
		[GtkChild] public unowned Gtk.ModelButton boutonModeGenerateur;
		[GtkChild] public unowned Gtk.ModelButton boutonModeReglages;
		[GtkChild] public unowned Gtk.ModelButton boutonMonitor;
		[GtkChild] public unowned Gtk.ModelButton boutonPwn;
		[GtkChild] public unowned Gtk.MenuButton boutonMenuMode;
		[GtkChild] public unowned Gtk.Entry texteAlias;
		[GtkChild] public unowned Gtk.Entry texteSecret;
		[GtkChild] public unowned Gtk.Entry passPwned;
		[GtkChild] public unowned Gtk.Button boutonAleatoire;
		[GtkChild] public unowned Gtk.Label motDePasse;
		[GtkChild] public unowned Gtk.Label labelFaille;
		[GtkChild] public unowned Gtk.Label motDePasseAleatoire;
		[GtkChild] public unowned Gtk.Label labelFailleAleatoire;
		[GtkChild] public unowned Gtk.Label labelFaillePwned;
		[GtkChild] public unowned Gtk.Label labelFaillePwned2;
		[GtkChild] public unowned Gtk.ComboBoxText comboHachage;
		[GtkChild] public unowned Gtk.Switch switchConversion;
		[GtkChild] public unowned Gtk.Entry texteSalage;
		[GtkChild] public unowned Gtk.Entry preAlias;
		[GtkChild] public unowned Gtk.Switch switchPwned;
		[GtkChild] public unowned Gtk.Switch switchQuitter;
		[GtkChild] public unowned Gtk.Switch switchNotifications;
		[GtkChild] public unowned Gtk.Scale scaleLongueur;
		[GtkChild] public unowned Gtk.Switch switchMinuscules;
		[GtkChild] public unowned Gtk.Switch switchMajuscules;
		[GtkChild] public unowned Gtk.Switch switchChiffres;
		[GtkChild] public unowned Gtk.Switch switchSpeciaux;
		[GtkChild] public unowned Gtk.Switch switchPin;
		[GtkChild] public unowned Gtk.Switch switchSuppression;
		[GtkChild] public unowned Gtk.Switch switchMasqueAlias;
		[GtkChild] public unowned Gtk.Scale scaleTemps;
		[GtkChild] public unowned WebKit.WebView web_view;
		[GtkChild] public unowned Gtk.Button boutonDeverrouillage;
		[GtkChild] public unowned Gtk.Entry entreeCode;
		[GtkChild] public unowned Gtk.Label labelCodeIncorrect;
        [GtkChild] public unowned Gtk.Button boutonVerrouillage;
		[GtkChild] public unowned Gtk.Entry entreeCode_1;
		[GtkChild] public unowned Gtk.Entry entreeCode_2;
		[GtkChild] public unowned Gtk.Label labelEntreeCodeIncorrect;
		[GtkChild] public unowned Gtk.Revealer revealVerrouillage;
		[GtkChild] public unowned Gtk.Switch switchVerrouillage;
		[GtkChild] public unowned Gtk.Image imageVerrouillage;
		[GtkChild] public unowned Gtk.Switch switchTheme;
		[GtkChild] public unowned Gtk.RadioButton modeClair;
		[GtkChild] public unowned Gtk.RadioButton modeSombre;
		[GtkChild] public unowned Gtk.ModelButton boutonTheme;
		[GtkChild] public unowned Gtk.Button boutonSupprimerProfil;
		[GtkChild] public unowned Gtk.Button boutonCreationProfil;
		[GtkChild] public unowned Gtk.ModelButton boutonProfil;
		[GtkChild] public unowned Gtk.ModelButton boutonProfilRetour;
		[GtkChild] public unowned Gtk.Box boxMenuProfil;

		//FIN WIDGETS

        public Window (Gtk.Application app) {

			Object (application: app);

            //INITIALISATION VARIABLES

            boutonQuitter.hide();//PLUS UTILISÉ
            boutonTheme.hide();//PLUS UTILISÉ
            switchTheme.hide();//PLUS UTILISÉ
            codeVerrouillage = "";
            passwordCourt = "";
            randomPassword = "";
            pass = "";
            dossierConfig = "";
            quitterAppli=false;
            attendreAvanDeFermer=false;
            notificationEffacement=false;
            phosh=false;
            largeur=-1;
            chgt=-1;
            champ=-1;
            url = "https://monitor.firefox.com/";
            nbProfils = 0;
            profilActuel = "";
            labelCodeIncorrect.hide();
            labelEntreeCodeIncorrect.hide();
            labelFaillePwned2.hide();
            verrou = false;
            boutonAleatoire.get_style_context().add_class(Gtk.STYLE_CLASS_SUGGESTED_ACTION);
            boutonVerrouillage.get_style_context().add_class(Gtk.STYLE_CLASS_SUGGESTED_ACTION);
            boutonPwned.get_style_context().add_class(Gtk.STYLE_CLASS_SUGGESTED_ACTION);
            boutonPwned2.get_style_context().add_class(Gtk.STYLE_CLASS_SUGGESTED_ACTION);
            boutonDeverrouillage.get_style_context().add_class(Gtk.STYLE_CLASS_DESTRUCTIVE_ACTION);
            boutonSupprimerProfil.get_style_context().add_class(Gtk.STYLE_CLASS_DESTRUCTIVE_ACTION);
            boutonSupprimerProfil.hide();
            boutonProfil.hide();


            //MODIF WIDGETS

            revealHaut.add(barre);
            texteAlias.set_alignment((float) 0.5);
            texteSecret.set_alignment((float) 0.5);
            motDePasseAleatoire.set_label("");
            boutonModeCalculateur.set_image(iconeCalculateur);
            boutonModeCalculateur.set_always_show_image(true);
            boutonModeCalculateur.set_label(_(" Calculator"));
            boutonModeGenerateur.set_image(iconeGenerateur);
            boutonModeGenerateur.set_always_show_image(true);
            boutonModeGenerateur.set_label(_(" Generator"));
            boutonModeReglages.set_image(iconeReglages);
            boutonModeReglages.set_always_show_image(true);
            boutonModeReglages.set_label(_(" Settings"));
            boutonMonitor.set_image(iconeMonitor);
            boutonMonitor.set_always_show_image(true);
            boutonMonitor.set_label(_(" Monitor"));
            boutonPwn.set_image(iconePwn);
            boutonPwn.set_always_show_image(true);
            boutonPwn.set_label(" HaveIBeenPwned");


            //TEST WAYLAND
            if(Environment.get_variable("WAYLAND_DISPLAY") != "wayland-0") boutonMonitor.hide();

            //LECTURE CLÉ UNIQUE

            cle1 = " ";
            cle2 = " ";
            uuid = " ";

            string? user = Environment.get_user_name();
            string? host = Environment.get_host_name();

            string fichierMachine = "/etc/machine-id";
            if(File.new_for_path(fichierMachine).query_exists()){
                var dis = FileStream.open (fichierMachine, "r");
                uuid = dis.read_line();
            }

            if(user.char_count()!=0) cle1 = user;
            if(host.char_count()!=0) cle2 = host;
            if(uuid.char_count()==0) uuid = " ";

            //CONFIGURATION
            //VERSION 1.0.9 : DÉPLACEMENT FICHIERS CONFIG VERS CONFIG/PASSWORD/.
            dossierConfig = Environment.get_user_config_dir()+"/password";
            if(!FileUtils.test(dossierConfig, FileTest.IS_DIR)) DirUtils.create(dossierConfig,0700);
            if(FileUtils.test(dossierConfig+"/../key.conf", FileTest.EXISTS) && !FileUtils.test(dossierConfig+"/key.conf", FileTest.EXISTS)){
                FileUtils.rename(dossierConfig+"/../key.conf", dossierConfig+"/key.conf");
            }
            if(FileUtils.test(dossierConfig+"/../cryptKey.conf", FileTest.EXISTS) && !FileUtils.test(dossierConfig+"/cryptKey.conf", FileTest.EXISTS)){
                FileUtils.rename(dossierConfig+"/../cryptKey.conf", dossierConfig+"/cryptKey.conf");
            }
            if(FileUtils.test(dossierConfig+"/../password.conf", FileTest.EXISTS) && !FileUtils.test(dossierConfig+"/password.conf", FileTest.EXISTS)){
                FileUtils.rename(dossierConfig+"/../password.conf", dossierConfig+"/password.conf");
            }
            if(FileUtils.test(dossierConfig+"/../theme.conf", FileTest.EXISTS) && !FileUtils.test(dossierConfig+"/theme.conf", FileTest.EXISTS)){
                FileUtils.rename(dossierConfig+"/../theme.conf", dossierConfig+"/theme.conf");
            }

            //PROFILS
            majListeProfils();
            //DERNIER PROFIL CHARGÉ
            if(nbProfils > 0){
                string fichierProfil = dossierConfig+"/profile.conf";
                if(File.new_for_path(fichierProfil).query_exists()){
                    var dis = FileStream.open (fichierProfil, "r");
                    string profil = dis.read_line();
                    bool profilExiste = false;
                    for(int i=0;i<nbProfils;i++){
                        if(listeProfils.nth_data((uint) i) == profil) profilExiste = true;
                    }
                    if(profilExiste) chargementProfil(profil);
                }
            }

            lectureConfig();

            if(switchPwned.get_state()) boutonPwned.hide();

            //MASQUAGE ALIAS
            unichar ch ='●';
            texteAlias.set_invisible_char(ch);
            texteAlias.set_visibility(!switchMasqueAlias.get_state());

            Pango.AttrList attrsEntrees = new Pango.AttrList ();
            attrsEntrees.insert (Pango.attr_family_new ("Monospace"));
            passPwned.set_attributes(attrsEntrees);
            passPwned.set_invisible_char(ch);
            passPwned.set_icon_from_icon_name(SECONDARY,"view-conceal-symbolic");


			//CONNEXIONS

            size_allocate.connect(chgtTaille);
            hide.connect(quitter);
            boutonModeCalculateur.clicked.connect(basculModeCalculateur);
            boutonModeGenerateur.clicked.connect(basculModeGenerateur);
            boutonModeReglages.clicked.connect(basculModeReglages);
            boutonMonitor.clicked.connect(basculMonitor);
            boutonPwn.clicked.connect(basculPwn);
            boutonAleatoire.clicked.connect(generation);
            switchMask.notify["active"].connect(basculMask);
            texteAlias.changed.connect(texteChange);
            texteAlias.icon_press.connect(() => {champ=0;on_icon_pressed();});
            texteAlias.activate.connect(() => {texteSecret.grab_focus();});
            texteSecret.changed.connect(texteChange);
            texteSecret.icon_press.connect(() => {champ=1;on_icon_pressed();});
            texteSecret.activate.connect(entreTexteSecret);
            boutonNettoyer.clicked.connect(nettoyer);
            boutonCopier.clicked.connect(copier);
            boutonPwned.clicked.connect(() => {if(fenetre.get_visible_child_name()=="pageCalculateur"){majFaille();}else{majFailleAleatoire();};});
            boutonPwned2.clicked.connect(majFaillePwned);
            //passPwned.icon_press.connect(() => {passPwned.set_text("");passPwned.grab_focus();});
            passPwned.icon_press.connect(passPwnedChgt);
            //passPwned.changed.connect(passPwnedChgt);
            passPwned.changed.connect(() => {labelFaillePwned.set_text("");labelFaillePwned2.hide();});
            passPwned.activate.connect(majFaillePwned);
            boutonPropos.clicked.connect(aPropos);
            comboHachage.changed.connect(() => {chgt=1;chgtReglages();});
            switchConversion.notify["active"].connect(() => {chgt=1;chgtReglages();});
            texteSalage.changed.connect(() => {chgt=1;chgtReglages();});
            texteSalage.icon_press.connect(() => {champ=2;on_icon_pressed();});
            preAlias.changed.connect(chgtPreAlias);
            preAlias.icon_press.connect(() => {champ=3;on_icon_pressed();});
            switchPwned.notify["active"].connect(() => {chgt=2;chgtReglages();if(switchPwned.get_state()){boutonPwned.hide();}else{boutonPwned.show();};});
            switchQuitter.notify["active"].connect(() => {chgt=0;chgtReglages();});
            switchNotifications.notify["active"].connect(() => {chgt=0;chgtReglages();});
            scaleLongueur.value_changed.connect(() => {chgt=2;chgtReglages();});
            switchMinuscules.notify["active"].connect(basculSwitchMinuscules);
            switchMajuscules.notify["active"].connect(basculSwitchMajuscules);
            switchChiffres.notify["active"].connect(basculSwitchChiffres);
            switchPin.notify["active"].connect(basculSwitchPin);
            switchSpeciaux.notify["active"].connect(() => {chgt=2;chgtReglages();});
            switchSuppression.notify["active"].connect(() => {chgt=0;chgtReglages();});
            switchMasqueAlias.notify["active"].connect(() => {chgt=0;chgtReglages();texteAlias.set_visibility(!switchMasqueAlias.get_state());});
            scaleTemps.value_changed.connect(() => {chgt=0;chgtReglages();});
            boutonReset.clicked.connect(reInitialisation);
            boutonQuitter.clicked.connect(quitter);
            boutonDeverrouillage.clicked.connect(deverrouillage);
            entreeCode.activate.connect(deverrouillage);
            entreeCode.icon_press.connect(() => {champ=0;visibiliteDeverrouillage();});
            switchVerrouillage.notify["active"].connect(verrouillage);
            entreeCode_1.changed.connect(entreeCode1Chgt);
            entreeCode_1.activate.connect(() => {entreeCode_2.grab_focus();});
            entreeCode_1.icon_press.connect(() => {champ=0;actionCodeVerrouillage();});
            entreeCode_2.changed.connect(entreeCode2Chgt);
            entreeCode_2.icon_press.connect(() => {entreeCode_2.set_text("");});
            entreeCode_2.activate.connect(entreeCodeVerrouillage);
            boutonVerrouillage.clicked.connect(entreeCodeVerrouillage);
            //boutonTheme.clicked.connect(() => {switchTheme.set_state(!switchTheme.get_state());});
            //switchTheme.notify["active"].connect(changeTheme);
            modeClair.notify["active"].connect(() => {changeTheme(true);});
            modeSombre.notify["active"].connect(() => {changeTheme(true);});
            boutonCreationProfil.clicked.connect(creationProfil);
            boutonSupprimerProfil.clicked.connect(suppressionProfil);
		}

		//SUBROUTINES

        void basculMask()
        {
            if(switchMask.get_active()==true){
                iconeSwitchMask.set_from_icon_name("view-reveal-symbolic",MENU);
                if(texteAlias.get_text()!="" && texteSecret.get_text()!="") motDePasse.set_label(calculMask(switchMask.get_state()));
                if(motDePasseAleatoire.get_text()!="") motDePasseAleatoire.set_label(mdpAleatoire(false, (int) scaleLongueur.get_value(), switchMask.get_state(),//
             switchMinuscules.get_state(), switchMajuscules.get_state(), switchChiffres.get_state(),//
             switchSpeciaux.get_state(), switchPin.get_state() )); //ON CHANGE L'AFFICHAGE DU MOT DE PASSE ALEATOIRE
            }
            else{
                iconeSwitchMask.set_from_icon_name("view-conceal-symbolic",MENU);
                if(texteAlias.get_text()!="" && texteSecret.get_text()!="") motDePasse.set_label(calculMask(switchMask.get_state()));
                if(motDePasseAleatoire.get_text()!="") motDePasseAleatoire.set_label(mdpAleatoire(false, (int) scaleLongueur.get_value(), switchMask.get_state(),//
             switchMinuscules.get_state(), switchMajuscules.get_state(), switchChiffres.get_state(),//
             switchSpeciaux.get_state(), switchPin.get_state() )); //ON CHANGE L'AFFICHAGE DU MOT DE PASSE ALEATOIRE
            }
        }

        void basculModeCalculateur()
        {
            iconeBoutonMenuMode.set_from_icon_name("accessories-calculator-symbolic", BUTTON);
            if(profilActuel == "") headerbar.set_subtitle(_("Password calculator"));
            if(profilActuel != "") headerbar.set_subtitle(_("Profile: ") + profilActuel);
            //modeCalculateur=true;modeGenerateur=false;
            if(!phosh) revealHaut.set_reveal_child(true);
            if(phosh) revealBarre.set_reveal_child(true);
            revealVerrouillage.set_reveal_child(false);
            if(passwordCourt=="" && revealCopier.get_child_revealed()) revealCopier.set_reveal_child(false);
            if(passwordCourt!="" && !revealCopier.get_child_revealed()) revealCopier.set_reveal_child(true);
            fenetre.set_visible_child(fenetreCalculateur);
        }

        void basculModeGenerateur()
        {
            iconeBoutonMenuMode.set_from_icon_name("applications-science-symbolic", BUTTON);
            if(profilActuel == "") headerbar.set_subtitle(_("Password generator"));
            if(profilActuel != "") headerbar.set_subtitle(_("Profile: ") + profilActuel);
            if(!phosh) revealHaut.set_reveal_child(true);
            if(phosh) revealBarre.set_reveal_child(true);
            revealVerrouillage.set_reveal_child(false);
            if(pass=="" && revealCopier.get_child_revealed()) revealCopier.set_reveal_child(false);
            if(pass!="" && !revealCopier.get_child_revealed()) revealCopier.set_reveal_child(true);
            fenetre.set_visible_child(fenetreGenerateur);
        }

        void basculModeReglages()
        {
            if(!phosh) revealHaut.set_reveal_child(false);
            if(phosh) revealBarre.set_reveal_child(false);
            if(!verrou){
                if(profilActuel == "") headerbar.set_subtitle(_("Settings"));
                if(profilActuel != "") headerbar.set_subtitle(_("Profile: ") + profilActuel);
                fenetre.set_visible_child(fenetreReglages);
                revealCopier.set_reveal_child(false);
                iconeBoutonMenuMode.set_from_icon_name("applications-system-symbolic", BUTTON);
                //VÉRIFICATION CODE VERROUILLAGE NON NUL : BUG SI ON A ANNULÉ LE VERROUILLAGE
                if(codeVerrouillage!=""){
                    imageVerrouillage.set_from_icon_name("changes-prevent-symbolic",BUTTON);
                }
                else{
                    imageVerrouillage.set_from_icon_name("changes-allow-symbolic",BUTTON);
                    switchVerrouillage.set_state(false);
                }
                revealVerrouillage.set_reveal_child(true);
                if(profilActuel!="") boutonSupprimerProfil.show();
                if(profilActuel=="") boutonSupprimerProfil.hide();
            }
            else{
                revealVerrouillage.set_reveal_child(false);
                labelCodeIncorrect.hide();
                iconeBoutonMenuMode.set_from_icon_name("changes-prevent-symbolic", BUTTON);
                if(profilActuel == "") headerbar.set_subtitle(_("Settings"));
                if(profilActuel != "") headerbar.set_subtitle(_("Profile: ") + profilActuel);
                entreeCode.set_text("");
                entreeCode.set_visibility(false);
                entreeCode.set_icon_from_icon_name(SECONDARY,"view-conceal-symbolic");
                fenetre.set_visible_child(fenetreDeverrouillage);
                revealCopier.set_reveal_child(false);
                entreeCode.grab_focus();
            }

        }

        void basculMonitor()
        {
            if(!phosh) revealHaut.set_reveal_child(false);
            if(phosh) revealBarre.set_reveal_child(false);
            revealVerrouillage.set_reveal_child(false);
            if(modeSombre.get_active()) iconeBoutonMenuMode.set_from_icon_name("firefox_monitor", BUTTON);
            if(!modeSombre.get_active()) iconeBoutonMenuMode.set_from_icon_name("firefox_monitor_light", BUTTON);
            headerbar.set_subtitle(_("Firefox Monitor"));
            fenetre.set_visible_child(web_view);
            revealCopier.set_reveal_child(false);
            web_view.load_uri(url);
        }

        void basculPwn()
        {
            if(!phosh) revealHaut.set_reveal_child(false);
            if(phosh) revealBarre.set_reveal_child(false);
            revealVerrouillage.set_reveal_child(false);
            if(modeSombre.get_active()) iconeBoutonMenuMode.set_from_icon_name("iconePwned", BUTTON);
            if(!modeSombre.get_active()) iconeBoutonMenuMode.set_from_icon_name("iconePwned_light", BUTTON);
            headerbar.set_subtitle("Password check");
            fenetre.set_visible_child(fenetrePwned);
            revealCopier.set_reveal_child(false);
        }

        void deverrouillage()
        {
            string code = entreeCode.get_text();

            if(code == codeVerrouillage){
                verrou = false;
                basculModeReglages();
            }
            else{
                labelCodeIncorrect.show();
                entreeCode.grab_focus();
            }
        }

        void visibiliteDeverrouillage()
        {
            if(entreeCode.get_visibility()){
                entreeCode.set_visibility(false);
                entreeCode.set_icon_from_icon_name(SECONDARY,"view-conceal-symbolic");
            }
            else{
                entreeCode.set_visibility(true);
                entreeCode.set_icon_from_icon_name(SECONDARY,"view-reveal-symbolic");
            }
        }

        void verrouillage()
        {
            if(switchVerrouillage.get_state()){
                imageVerrouillage.set_from_icon_name("changes-prevent-symbolic", BUTTON);
                revealVerrouillage.set_reveal_child(false);
                iconeBoutonMenuMode.set_from_icon_name("changes-prevent-symbolic", BUTTON);
                entreeCode_1.set_text("");
                entreeCode_2.set_text("");
                labelEntreeCodeIncorrect.hide();
                entreeCode_1.set_icon_from_icon_name(SECONDARY,"system-run-symbolic");
                entreeCode_2.set_icon_from_icon_name(SECONDARY,null);
                fenetre.set_visible_child(fenetreVerrouillage);
                entreeCode_1.grab_focus();
            }
            else{
                imageVerrouillage.set_from_icon_name("changes-allow-symbolic", BUTTON);
                if(listeProfils.length() > 0) dechiffrementProfils();
                codeVerrouillage = "";
                FileUtils.remove(dossierConfig + "/cryptKey.conf");
                ecritureConfig(profilActuel);//REECRITURE FICHIER CONFIG EN CLAIR
            }
        }

        void entreeCode1Chgt()
        {
            if(entreeCode_1.get_text()==""){//INIT
                entreeCode_1.set_icon_from_icon_name(SECONDARY,"system-run-symbolic");
                entreeCode_1.set_visibility(false);
            }
            else if(entreeCode_1.get_text()!="" && entreeCode_1.get_icon_name(SECONDARY)!="edit-delete-symbolic"){
                entreeCode_1.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            }
        }

        void entreeCode2Chgt()
        {
            if(entreeCode_2.get_text()==""){//INIT
                entreeCode_2.set_icon_from_icon_name(SECONDARY,null);
            }
            else if(entreeCode_2.get_text()!="" && entreeCode_2.get_icon_name(SECONDARY)==null){
                entreeCode_2.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            }
        }

        void entreeCodeVerrouillage(){
            bool test = false;
            entreeCode_1.set_visibility(false);
            string code1 = entreeCode_1.get_text();
            string code2 = entreeCode_2.get_text();
            if(code1.char_count() == 6 && code2.char_count() == 6 && code1 == code2){
                bool testNum = true;
                //VERIFICATION QUE CODE1 EST COMPOSÉ UNIQUEMENT DE CHIFFRE
                for(int i=0;i<6;i++){
                    if(!code1.get_char(i).isdigit() && testNum) testNum = false;
                }
                if(testNum){
                    codeVerrouillage = code1;
                    basculModeReglages();
                    test = true;
                    ecritureCle();
                    ecritureCryptConfig(profilActuel);//REECRITURE FICHIER CONFIG CHIFFRÉ
                    if(listeProfils.length() > 0) chiffrementProfils();
                }
            }
            if(!test){
                entreeCode_1.grab_focus();
                labelEntreeCodeIncorrect.show();
            }
        }

        void actionCodeVerrouillage(){
            if(entreeCode_1.get_icon_name(SECONDARY)=="system-run-symbolic"){
                entreeCode_1.set_visibility(true);
                entreeCode_1.set_text(mdpAleatoire(true, 6, true,false,false,false,false,true));
                entreeCode_2.set_text(entreeCode_1.get_text());
            }
            else if(entreeCode_1.get_icon_name(SECONDARY)=="edit-delete-symbolic"){
                entreeCode_1.set_text("");
            }
        }



        void texteChange()
        {
            labelFaille.set_label("");
             if(texteAlias.get_text()!="" || texteSecret.get_text()!=""){ //SI AU MOINS UN DES DEUX REMPLI
                   iconeBoutonNettoyer.set_from_icon_name("user-trash-full-symbolic", BUTTON);
                   if(texteAlias.get_text()!="" && texteAlias.get_icon_name(SECONDARY)==null) texteAlias.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
                   if(texteSecret.get_text()!="" && texteSecret.get_icon_name(SECONDARY)==null) texteSecret.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
               }
             if(texteAlias.get_text()=="" || texteSecret.get_text()==""){ //SI AU MOINS UN DES DEUX VIDES
                    motDePasse.set_label(_("your password"));
                    passwordCourt="";
                    revealCopier.set_reveal_child(false);
                    if(texteAlias.get_text()=="" && texteAlias.get_icon_name(SECONDARY)!=null) texteAlias.set_icon_from_icon_name(SECONDARY,null);
                    if(texteSecret.get_text()=="" && texteSecret.get_icon_name(SECONDARY)!=null) texteSecret.set_icon_from_icon_name(SECONDARY,null);
                    }

             if(texteAlias.get_text()=="" && texteSecret.get_text()==""){ //SI LES DEUX VIDES
                   if(motDePasseAleatoire.get_label()=="") iconeBoutonNettoyer.set_from_icon_name("user-trash-symbolic", BUTTON);
                   motDePasse.set_label(_("your password"));
                   passwordCourt="";
                   revealCopier.set_reveal_child(false);
               }
             if(texteAlias.get_text()!="" && texteSecret.get_text()!=""){ //SI LES DEUX REMPLIS
                    calcul();
               }

        }

        void on_icon_pressed()
        {
            if(champ==0) texteAlias.set_text("");
            if(champ==1) texteSecret.set_text("");
            if(champ==2) texteSalage.set_text("");
            if(champ==3) preAlias.set_text("");
        }

        void calcul()
        {
            motDePasse.set_label(mdpCalcul(texteAlias.get_text(), texteSecret.get_text(), switchMask.get_state(),//
                comboHachage.get_active(), switchConversion.get_state(), (int) scaleLongueur.get_value(),//
                texteSalage.get_text(), switchMinuscules.get_state(), switchMajuscules.get_state(),//
                switchChiffres. get_state(), switchSpeciaux.get_state(), switchPin.get_state(), false ));
            revealCopier.set_reveal_child(true);
            if(switchPwned.get_state()) majFaille();
        }

        void generation()
        {
            iconeBoutonNettoyer.set_from_icon_name("user-trash-full-symbolic", BUTTON);
            motDePasseAleatoire.set_label(mdpAleatoire(true, (int) scaleLongueur.get_value(), switchMask.get_state(),//
                switchMinuscules.get_state(), switchMajuscules.get_state(), switchChiffres.get_state(),//
                switchSpeciaux.get_state(),switchPin.get_state() ));
            revealCopier.set_reveal_child(true);
            if(switchPwned.get_state()) majFailleAleatoire();
            else{labelFailleAleatoire.set_label("");}
        }

        void majFaille()
        {
            //HAVEIBEENPWNED
            int faille = appelCheckPwned(passwordCourt);
            if(faille > 0){
                labelFaille.set_label(_("Be careful!\nThis password appears ")+ faille.to_string() + _(" times in HaveIBeenPwned database"));
                var rouge = "* { color: #F53C32; }";
                var p1 = new Gtk.CssProvider();
                try{p1.load_from_data(rouge, rouge.length);} catch (Error err) {}
                labelFaille.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            }
            else if(faille == 0){
                labelFaille.set_label(_("This password does not appear in HaveIBeenPwned database"));
                var vert = "* { color: #00AB63; }";
                var p1 = new Gtk.CssProvider();
                try{p1.load_from_data(vert, vert.length);} catch (Error err) {}
                labelFaille.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            }
            else{//FAILLE = -1 : PAS DE CONNEXION INTERNET
                labelFaille.set_label(_("Check your internet connection!"));
                var rouge = "* { color: #F53C32; }";
                var p1 = new Gtk.CssProvider();
                try{p1.load_from_data(rouge, rouge.length);} catch (Error err) {}
                labelFaille.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            }
        }

        void majFailleAleatoire()
        {
            //HAVEIBEENPWNED
            int faille = appelCheckPwned(pass);
            if(faille > 0){
                labelFailleAleatoire.set_label(_("Be careful!\nThis password appears ")+ faille.to_string() + _(" times in HaveIBeenPwned database"));
                var rouge = "* { color: #F53C32; }";
                var p1 = new Gtk.CssProvider();
                try{p1.load_from_data(rouge, rouge.length);} catch (Error err) {}
                labelFailleAleatoire.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            }
            else if(faille == 0){
                labelFailleAleatoire.set_label(_("This password does not appear in HaveIBeenPwned database"));
                var vert = "* { color: #00AB63; }";
                var p1 = new Gtk.CssProvider();
                try{p1.load_from_data(vert, vert.length);} catch (Error err) {}
                labelFailleAleatoire.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            }
            else{//FAILLE = -1 : PAS DE CONNEXION INTERNET
                labelFailleAleatoire.set_label(_("Check your internet connection!"));
                var rouge = "* { color: #F53C32; }";
                var p1 = new Gtk.CssProvider();
                try{p1.load_from_data(rouge, rouge.length);} catch (Error err) {}
                labelFailleAleatoire.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            }
        }

        void passPwnedChgt()
        {
            /*labelFaillePwned.set_text("");
            labelFaillePwned2.hide();
            if(passPwned.get_text()!=""){
                if(passPwned.get_icon_name(SECONDARY)==null) passPwned.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            }
            else{
                passPwned.set_icon_from_icon_name(SECONDARY,null);
            }*/
            if(passPwned.get_icon_name(SECONDARY)=="view-conceal-symbolic"){
                passPwned.set_icon_from_icon_name(SECONDARY,"view-reveal-symbolic");
                passPwned.set_visibility(false);
            }
            else{
                passPwned.set_icon_from_icon_name(SECONDARY,"view-conceal-symbolic");
                passPwned.set_visibility(true);
            }

        }

        void majFaillePwned()
        {
             labelFaillePwned.set_label("");
             labelFaillePwned2.hide();
             if(passPwned.get_text()!=""){
                 int faille = appelCheckPwned(passPwned.get_text());
                 if(faille > 0){
                    labelFaillePwned.set_label(_("Be careful!\nThis password appears ")+ faille.to_string() + _(" times in HaveIBeenPwned database"));
                    var rouge = "* { color: #F53C32; }";
                    var p1 = new Gtk.CssProvider();
                    try{p1.load_from_data(rouge, rouge.length);} catch (Error err) {}
                    labelFaillePwned.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    labelFaillePwned2.show();
                }
                else if(faille==0){
                    labelFaillePwned.set_label(_("This password does not appear in HaveIBeenPwned database"));
                    var vert = "* { color: #00AB63; }";
                    var p1 = new Gtk.CssProvider();
                    try{p1.load_from_data(vert, vert.length);} catch (Error err) {}
                    labelFaillePwned.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                }
                else{//FAILLE = -1 : PAS DE CONNEXION INTERNET
                    labelFaillePwned.set_label(_("Check your internet connection!"));
                    var rouge = "* { color: #F53C32; }";
                    var p1 = new Gtk.CssProvider();
                    try{p1.load_from_data(rouge, rouge.length);} catch (Error err) {}
                    labelFaillePwned.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                 }
             }
        }

        void chgtPreAlias()
        {
            texteAlias.set_text(preAlias.get_text());
            chgt=0;
            chgtReglages();
        }

        void chgtReglages()
        {
            // CHGT=0 ; ENREGISTREMENT UNIQUEMENT
            // CHGT=1 ; ENREGISTREMENT + CALCUL
            // CHGT=2 ; ENREGISTREMENT + CALCUL + ALEATOIRE

            if(codeVerrouillage == "") ecritureConfig(profilActuel);
            if(codeVerrouillage != "") ecritureCryptConfig(profilActuel);

            if(preAlias.get_text()!="" && preAlias.get_icon_name(SECONDARY)==null) preAlias.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            if(preAlias.get_text()=="" && preAlias.get_icon_name(SECONDARY)!=null) preAlias.set_icon_from_icon_name(SECONDARY,null);
            if(texteSalage.get_text()!="" && texteSalage.get_icon_name(SECONDARY)==null) texteSalage.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            if(texteSalage.get_text()=="" && texteSalage.get_icon_name(SECONDARY)!=null) texteSalage.set_icon_from_icon_name(SECONDARY,null);

            if(chgt>=1 && texteAlias.get_text()!="" && texteSecret.get_text()!=""){
                motDePasse.set_label(mdpCalcul(texteAlias.get_text(), texteSecret.get_text(), switchMask.get_state(),//
                    comboHachage.get_active(), switchConversion.get_state(), (int) scaleLongueur.get_value(),//
                    texteSalage.get_text(), switchMinuscules.get_state(), switchMajuscules.get_state(),//
                    switchChiffres. get_state(), switchSpeciaux.get_state(), switchPin.get_state(), false ));
                if(switchPwned.get_state()) majFaille();
                else{labelFaille.set_label("");}
            }
            if(chgt==2 && motDePasseAleatoire.get_text()!=""){
                motDePasseAleatoire.set_label(mdpAleatoire(false, (int) scaleLongueur.get_value(), switchMask.get_state(),//
                    switchMinuscules.get_state(), switchMajuscules.get_state(), switchChiffres.get_state(),//
                    switchSpeciaux.get_state(), switchPin.get_state() )); //ON CHANGE L'AFFICHAGE DU MOT DE PASSE ALEATOIRE
                if(switchPwned.get_state()) majFailleAleatoire();
                else{labelFailleAleatoire.set_label("");}
            }

            forceMotDePasse();

        }

        void forceMotDePasse()
        {
            int force = calculForce((int) scaleLongueur.get_value(), switchMinuscules.get_state(), switchMajuscules.get_state(), switchChiffres.get_state(), switchSpeciaux.get_state(), switchPin.get_state());
            var rouge = "* { background: #F53C32; }";// #E90000; }";
            var orange = "* { background: #FFCD03; }";//#E99600; }";
            var bleu = "* { background: #00C2F0; }";
            var vert = "* { background: #00AB63; }";//#00D200; }";
            var transparent = "* { background: transparent; }";
            var p1 = new Gtk.CssProvider();
            var p2 = new Gtk.CssProvider();

            if(force < 64){
                iconeBouclier.set_from_icon_name("bouclier_0",DIALOG);
                labelForce.set_text(_("Very weak password") + " [" + force.to_string() + " bits]");
                try {
                    p1.load_from_data(rouge, rouge.length);
                    p2.load_from_data(transparent, transparent.length);
                    barre0.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre1.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre2.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre3.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre4.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    } catch (Error err) {
                }
            }
            else if(force < 80){
                iconeBouclier.set_from_icon_name("bouclier_1",DIALOG);
                labelForce.set_text(_("Weak password") + " [" + force.to_string() + " bits]");
                try {
                    p1.load_from_data(orange, orange.length);
                    p2.load_from_data(transparent, transparent.length);
                    barre0.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre1.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre2.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre3.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre4.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    } catch (Error err) {
                }
            }
            else if(force < 100){
                iconeBouclier.set_from_icon_name("bouclier_2",DIALOG);
                labelForce.set_text(_("Fairly strong password") + " [" + force.to_string() + " bits]");
                try {
                    p1.load_from_data(bleu, bleu.length);
                    p2.load_from_data(transparent, transparent.length);
                    barre0.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre1.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre2.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre3.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre4.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    } catch (Error err) {
                }
            }
            else if(force < 128){
                iconeBouclier.set_from_icon_name("bouclier_3",DIALOG);
                labelForce.set_text(_("Strong password") + " [" + force.to_string() + " bits]");
                try {
                    p1.load_from_data(vert, vert.length);
                    p2.load_from_data(transparent, transparent.length);
                    barre0.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre1.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre2.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre3.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre4.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    } catch (Error err) {
                }
            }
            else{
                iconeBouclier.set_from_icon_name("bouclier_4",DIALOG);
                labelForce.set_text(_("Very strong password") + " [≥ 128 bits]");
                try {
                    p1.load_from_data(vert, vert.length);
                    barre0.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre1.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre2.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre3.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    barre4.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    } catch (Error err) {
                }
            }
        }

        void basculSwitchMinuscules()
        {
            if(switchMinuscules.get_state() && switchMajuscules.get_state()){
                switchMajuscules.set_state(false);
            }
            else{
                chgt=2;
                chgtReglages();
            }
        }

        void basculSwitchMajuscules()
        {
            if(switchMajuscules.get_state() && switchMinuscules.get_state()){
                switchMinuscules.set_state(false);
            }
            else{
                chgt=2;
                chgtReglages();
            }
        }

        void basculSwitchChiffres()
        {
            if(switchChiffres.get_state() && switchPin.get_state()){
                switchPin.set_state(false);
            }
            else{
                chgt=2;
                chgtReglages();
            }
        }

        void basculSwitchPin()
        {
            if(switchPin.get_state() && switchChiffres.get_state()){
                switchChiffres.set_state(false);
            }
            else{
                chgt=2;
                chgtReglages();
            }
        }

        void lectureConfig()
        {
            string fichierCle = dossierConfig + "/key.conf";
            string fichierCleChiffree = dossierConfig + "/cryptKey.conf";
            string fichierConfig = dossierConfig + "/password.conf";
            string fichierTheme = dossierConfig + "/theme.conf";

            if(File.new_for_path(fichierCle).query_exists()){
                codeVerrouillage = "";
                var dis = FileStream.open (fichierCle, "r");
                codeVerrouillage = dis.read_line();
                if(codeVerrouillage.char_count() == 6){
                    bool testNum = true;
                    //VERIFICATION CODE EST COMPOSÉ UNIQUEMENT DE CHIFFRE
                    for(int i=0;i<6;i++){
                        if(!codeVerrouillage.get_char(i).isdigit() && testNum) testNum = false;
                    }
                    if(testNum){
                        verrou = true;
                    }
                    else{
                        codeVerrouillage = "";
                        verrou = false;
                    }
                }
                //ANCIENNE CONFIG, ON CHIFFRE LA CLÉ
                ecritureCle();
                lectureFichierConfig();
                ecritureCryptConfig(profilActuel);
                FileUtils.remove(dossierConfig + "/key.conf");
            }

            if(File.new_for_path(fichierCleChiffree).query_exists()){
                //CREATION CLÉ DE CHIFFREMENT UNIQUE
                string cle = mdpCalcul(cle1,cle2, true, 4, true, 12 ,uuid, false , false , false , false , true, true);
                //LECTURE CLÉ CHIFFRÉE
                codeVerrouillage = "";
                var dis = FileStream.open (fichierCleChiffree, "r");
                string cleChiffree = dis.read_line();
                for(int i=0;i<6;i++){
                    unichar ch = cleChiffree.get_char(cleChiffree.index_of_nth_char(i));
                    int32 crypt = (int32) int.parse(cle.slice(2*i,2*i+2));
                    ch -= crypt;
                    codeVerrouillage += ch.to_string();
                }

                verrou = true;
                imageVerrouillage.set_from_icon_name("changes-prevent-symbolic", BUTTON);
            }

            switchVerrouillage.set_state(verrou);

            if(File.new_for_path(fichierConfig).query_exists()){
                if(codeVerrouillage == "") lectureFichierConfig();
                if(codeVerrouillage != "") lectureFichierCryptConfig();
            }

            if(File.new_for_path(fichierTheme).query_exists()){
                var dis = FileStream.open (fichierTheme, "r");
                modeSombre.set_active(bool.parse(dis.read_line()));
                //Gtk.Settings.get_default().set("gtk-application-prefer-dark-theme", modeSombre.get_active());
                changeTheme(false);
            }

            //PRE-REMPLISSAGE ALIAS
            if(preAlias.get_text()!=""){
                texteAlias.set_text(preAlias.get_text());
                texteSecret.grab_focus();
                texteAlias.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
                preAlias.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            }
            if(texteSalage.get_text()!="") texteSalage.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");

            forceMotDePasse();
        }

        void lectureFichierConfig()
        {
            string fichierConfig = dossierConfig + "/password.conf";
            var dis = FileStream.open(fichierConfig, "r");
            texteSalage.set_text(dis.read_line());
            preAlias.set_text(dis.read_line());
            comboHachage.set_active(int.parse(dis.read_line()));
            switchConversion.set_state(bool.parse(dis.read_line()));
            switchQuitter.set_state(bool.parse(dis.read_line()));
            scaleLongueur.set_value(int.parse(dis.read_line()));
            switchMinuscules.set_state(bool.parse(dis.read_line()));
            switchMajuscules.set_state(bool.parse(dis.read_line()));
            switchChiffres.set_state(bool.parse(dis.read_line()));
            switchSpeciaux.set_state(bool.parse(dis.read_line()));
            switchSuppression.set_state(bool.parse(dis.read_line()));
            scaleTemps.set_value(int.parse(dis.read_line()));
            switchNotifications.set_state(bool.parse(dis.read_line()));
            switchPin.set_state(bool.parse(dis.read_line()));
            switchMasqueAlias.set_state(bool.parse(dis.read_line()));
            switchPwned.set_state(bool.parse(dis.read_line()));

            if(profilActuel!=""){
                string fichierConfigProfil = dossierConfig + "/password-" + profilActuel + ".conf";
                var disProfil = FileStream.open(fichierConfigProfil, "r");
                texteSalage.set_text(disProfil.read_line());
                preAlias.set_text(disProfil.read_line());
                comboHachage.set_active(int.parse(disProfil.read_line()));
                switchConversion.set_state(bool.parse(disProfil.read_line()));
                scaleLongueur.set_value(int.parse(disProfil.read_line()));
                switchMinuscules.set_state(bool.parse(disProfil.read_line()));
                switchMajuscules.set_state(bool.parse(disProfil.read_line()));
                switchChiffres.set_state(bool.parse(disProfil.read_line()));
                switchSpeciaux.set_state(bool.parse(disProfil.read_line()));
                switchPin.set_state(bool.parse(disProfil.read_line()));
            }
        }

        void lectureFichierCryptConfig()
        {
            string fichierConfig = dossierConfig + "/password.conf";
            var dis = FileStream.open (fichierConfig, "r");
            string cle = mdpCalcul(codeVerrouillage,codeVerrouillage, true, 4, true, 42 ,"", false , false , false , false , true, true);
            texteSalage.set_text(chiffrement(dis.read_line(),cle.slice(0,2),-1,false));
            preAlias.set_text(chiffrement(dis.read_line(),cle.slice(2,4),-1,false));
            comboHachage.set_active(int.parse(chiffrement(dis.read_line(),cle.slice(2,3),-1,false)));
            switchConversion.set_state(bool.parse(chiffrement(dis.read_line(),cle.slice(3,4),-1,true)));
            switchQuitter.set_state(bool.parse(chiffrement(dis.read_line(),cle.slice(4,5),-1,true)));
            scaleLongueur.set_value(int.parse(chiffrement(dis.read_line(),cle.slice(5,6),-1,false)));
            switchMinuscules.set_state(bool.parse(chiffrement(dis.read_line(),cle.slice(6,7),-1,true)));
            switchMajuscules.set_state(bool.parse(chiffrement(dis.read_line(),cle.slice(7,8),-1,true)));
            switchChiffres.set_state(bool.parse(chiffrement(dis.read_line(),cle.slice(8,9),-1,true)));
            switchSpeciaux.set_state(bool.parse(chiffrement(dis.read_line(),cle.slice(9,10),-1,true)));
            switchSuppression.set_state(bool.parse(chiffrement(dis.read_line(),cle.slice(10,11),-1,true)));
            scaleTemps.set_value(int.parse(chiffrement(dis.read_line(),cle.slice(11,12),-1,false)));
            switchNotifications.set_state(bool.parse(chiffrement(dis.read_line(),cle.slice(12,13),-1,true)));
            switchPin.set_state(bool.parse(chiffrement(dis.read_line(),cle.slice(13,14),-1,true)));
            switchMasqueAlias.set_state(bool.parse(chiffrement(dis.read_line(),cle.slice(14,15),-1,true)));
            switchPwned.set_state(bool.parse(chiffrement(dis.read_line(),cle.slice(15,16),-1,true)));

            if(profilActuel!=""){
                string fichierConfigProfil = dossierConfig + "/password-" + profilActuel + ".conf";
                var disProfil = FileStream.open (fichierConfigProfil, "r");
                texteSalage.set_text(chiffrement(disProfil.read_line(),cle.slice(0,2),-1,false));
                preAlias.set_text(chiffrement(disProfil.read_line(),cle.slice(2,4),-1,false));
                comboHachage.set_active(int.parse(chiffrement(disProfil.read_line(),cle.slice(2,3),-1,false)));
                switchConversion.set_state(bool.parse(chiffrement(disProfil.read_line(),cle.slice(3,4),-1,true)));
                scaleLongueur.set_value(int.parse(chiffrement(disProfil.read_line(),cle.slice(5,6),-1,false)));
                switchMinuscules.set_state(bool.parse(chiffrement(disProfil.read_line(),cle.slice(6,7),-1,true)));
                switchMajuscules.set_state(bool.parse(chiffrement(disProfil.read_line(),cle.slice(7,8),-1,true)));
                switchChiffres.set_state(bool.parse(chiffrement(disProfil.read_line(),cle.slice(8,9),-1,true)));
                switchSpeciaux.set_state(bool.parse(chiffrement(disProfil.read_line(),cle.slice(9,10),-1,true)));
                switchPin.set_state(bool.parse(chiffrement(disProfil.read_line(),cle.slice(13,14),-1,true)));
            }
        }

        void ecritureConfig(string profil)
        {
            string fichierConfig = dossierConfig + "/password.conf";
            string config = "";
            config += texteSalage.get_text() + "\n";
            config += preAlias.get_text() + "\n";
            config += comboHachage.get_active().to_string() + "\n";
            config += switchConversion.get_state().to_string() + "\n";
            config += switchQuitter.get_state().to_string() + "\n";
            config += scaleLongueur.get_value().to_string() + "\n";
            config += switchMinuscules.get_state().to_string() + "\n";
            config += switchMajuscules.get_state().to_string() + "\n";
            config += switchChiffres.get_state().to_string() + "\n";
            config += switchSpeciaux.get_state().to_string() + "\n";
            config += switchSuppression.get_state().to_string() + "\n";
            config += scaleTemps.get_value().to_string() + "\n";
            config += switchNotifications.get_state().to_string() + "\n";
            config += switchPin.get_state().to_string() + "\n";
            config += switchMasqueAlias.get_state().to_string() + "\n";
            config += switchPwned.get_state().to_string() + "\n";
            var dis = FileStream.open (fichierConfig, "w");
            dis.write(config.data);

            if(profil != ""){
                fichierConfig = dossierConfig + "/password-"+profil+".conf";
                config = "";
                config += texteSalage.get_text() + "\n";
                config += preAlias.get_text() + "\n";
                config += comboHachage.get_active().to_string() + "\n";
                config += switchConversion.get_state().to_string() + "\n";
                config += scaleLongueur.get_value().to_string() + "\n";
                config += switchMinuscules.get_state().to_string() + "\n";
                config += switchMajuscules.get_state().to_string() + "\n";
                config += switchChiffres.get_state().to_string() + "\n";
                config += switchSpeciaux.get_state().to_string() + "\n";
                config += switchPin.get_state().to_string() + "\n";
                var disProfil = FileStream.open (fichierConfig, "w");
                disProfil.write(config.data);
            }
        }

        void ecritureCryptConfig(string profil)
        {
            string fichierConfig = dossierConfig + "/password.conf";
            string config = "";
            string cle = mdpCalcul(codeVerrouillage,codeVerrouillage, true, 4, true, 42 ,"", false , false , false , false , true, true);
            config += chiffrement(texteSalage.get_text(),cle.slice(0,2),1,false) + "\n";
            config += chiffrement(preAlias.get_text(),cle.slice(2,4),1,false) + "\n";
            config += chiffrement(comboHachage.get_active().to_string(),cle.slice(2,3),1,false) + "\n";
            config += chiffrement(switchConversion.get_state().to_string(),cle.slice(3,4),1,true) + "\n";
            config += chiffrement(switchQuitter.get_state().to_string(),cle.slice(4,5),1,true) + "\n";
            config += chiffrement(scaleLongueur.get_value().to_string(),cle.slice(5,6),1,false) + "\n";
            config += chiffrement(switchMinuscules.get_state().to_string(),cle.slice(6,7),1,true) + "\n";
            config += chiffrement(switchMajuscules.get_state().to_string(),cle.slice(7,8),1,true) + "\n";
            config += chiffrement(switchChiffres.get_state().to_string(),cle.slice(8,9),1,true) + "\n";
            config += chiffrement(switchSpeciaux.get_state().to_string(),cle.slice(9,10),1,true) + "\n";
            config += chiffrement(switchSuppression.get_state().to_string(),cle.slice(10,11),1,true) + "\n";
            config += chiffrement(scaleTemps.get_value().to_string(),cle.slice(11,12),1,false) + "\n";
            config += chiffrement(switchNotifications.get_state().to_string(),cle.slice(12,13),1,true) + "\n";
            config += chiffrement(switchPin.get_state().to_string(),cle.slice(13,14),1,true) + "\n";
            config += chiffrement(switchMasqueAlias.get_state().to_string(),cle.slice(14,15),1,true) + "\n";
            config += chiffrement(switchPwned.get_state().to_string(),cle.slice(14,15),1,true) + "\n";
            var dis = FileStream.open (fichierConfig, "w");
            dis.write(config.data);


            if(profil != ""){
                fichierConfig = dossierConfig + "/password-"+profil+".conf";
                config = "";
                config += chiffrement(texteSalage.get_text(),cle.slice(0,2),1,false) + "\n";
                config += chiffrement(preAlias.get_text(),cle.slice(2,4),1,false) + "\n";
                config += chiffrement(comboHachage.get_active().to_string(),cle.slice(2,3),1,false) + "\n";
                config += chiffrement(switchConversion.get_state().to_string(),cle.slice(3,4),1,true) + "\n";
                config += chiffrement(scaleLongueur.get_value().to_string(),cle.slice(5,6),1,false) + "\n";
                config += chiffrement(switchMinuscules.get_state().to_string(),cle.slice(6,7),1,true) + "\n";
                config += chiffrement(switchMajuscules.get_state().to_string(),cle.slice(7,8),1,true) + "\n";
                config += chiffrement(switchChiffres.get_state().to_string(),cle.slice(8,9),1,true) + "\n";
                config += chiffrement(switchSpeciaux.get_state().to_string(),cle.slice(9,10),1,true) + "\n";
                config += chiffrement(switchPin.get_state().to_string(),cle.slice(13,14),1,true) + "\n";
                var disProfil = FileStream.open (fichierConfig, "w");
                disProfil.write(config.data);
            }
        }

        string chiffrement(string texte, string cle, int sens, bool varSwitch)
        {
            string resultat = "";
            if(texte!=""){
                if(varSwitch && sens == 1 && texte == "true") texte = "1";
                if(varSwitch && sens == 1 && texte == "false") texte = "0";
                for(int i=0;i<texte.char_count();i++){
                    int32 crypt = (int32) sens*int.parse(cle);
                    unichar ch = texte.get_char(texte.index_of_nth_char(i));
                    ch += crypt;
                    resultat += ch.to_string();
                }
                if(varSwitch && sens == -1 && resultat == "1") resultat = "true";
                if(varSwitch && sens == -1 && resultat == "0") resultat = "false";
            }
            return resultat;
        }

        void ecritureCle()
        {
            string fichierCle = dossierConfig + "/cryptKey.conf";
            string config = "";
            //CREATION CLÉ DE CHIFFREMENT UNIQUE
            string cle = mdpCalcul(cle1,cle2, true, 4, true, 12 ,uuid, false , false , false , false , true, true);
            for(int i=0;i<6;i++){
                int32 crypt = int.parse(cle.slice(2*i,2*i+2));
                unichar ch = codeVerrouillage.get_char(codeVerrouillage.index_of_nth_char(i));
                ch += crypt;
                config += ch.to_string();
            }
            config += "\n";
            var dis = FileStream.open (fichierCle, "w");
            dis.write(config.data);
        }

        void reInitialisation()
        {
            //AVERTISSEMENT
            Gtk.MessageDialog confirmation;
            if(listeProfils.length()>0){
                confirmation = new Gtk.MessageDialog(this, Gtk.DialogFlags.MODAL, Gtk.MessageType.WARNING,Gtk.ButtonsType.NONE ,_("Are you sure you want to delete the settings and all the profiles?"));
            }
            else{
                confirmation = new Gtk.MessageDialog(this, Gtk.DialogFlags.MODAL, Gtk.MessageType.WARNING,Gtk.ButtonsType.NONE ,_("Are you sure you want to delete the settings?"));
            }
            Gtk.Widget annuler;
            annuler = confirmation.add_button(_("Cancel"),Gtk.ResponseType.REJECT);
            Gtk.Widget effacer;
            effacer = confirmation.add_button(_("Delete"),Gtk.ResponseType.ACCEPT);
            effacer.get_style_context().add_class(Gtk.STYLE_CLASS_DESTRUCTIVE_ACTION);
            //confirmation.add_buttons(_("test1"),Gtk.ResponseType.REJECT,_("test2"),Gtk.ResponseType.ACCEPT);
            Gtk.ResponseType result = (Gtk.ResponseType)confirmation.run ();
            confirmation.close();

            if (result == Gtk.ResponseType.ACCEPT){
                comboHachage.set_active(0);
                switchConversion.set_state(false);
                texteSalage.set_text("");
                preAlias.set_text("");
                switchQuitter.set_state(false);
                scaleLongueur.set_value(12);
                switchMinuscules.set_state(false);
                switchMajuscules.set_state(false);
                switchChiffres.set_state(false);
                switchPin.set_state(false);
                switchSpeciaux.set_state(false);
                switchSuppression.set_state(false);
                scaleTemps.set_value(5);
                switchNotifications.set_state(false);
                switchVerrouillage.set_state(false);
                switchMasqueAlias.set_state(false);
                codeVerrouillage="";
                verrou = false;
                FileUtils.remove(dossierConfig + "/password.conf");
                FileUtils.remove(dossierConfig + "/cryptKey.conf");
                FileUtils.remove(dossierConfig + "/key.conf");
                FileUtils.remove(dossierConfig + "/profile.conf");
                for(int i=0;i<listeProfils.length();i++){
                    FileUtils.remove(dossierConfig + "/password-" + listeProfils.nth_data((uint) i) + ".conf");
                }
                majListeProfils();
                profilActuel = "";
                basculModeReglages();
            }
        }

        void aPropos()
        {
            //Gdk.Pixbuf *password_logo = new Gdk.Pixbuf.from_resource ("/org/emilien/password/org.emilien.password.svg");
            const string COPYRIGHT = "Copyright \xc2\xa9 2020-2021 Emilien Lescoute.\n";
            const string AUTHORS[] = {
                "Emilien Lescoute <emilien.lescoute@lilo.org>",
                null
            };
            var program_name = "Password";
            Gtk.show_about_dialog (this,
                               "program-name", program_name,
                               "logo-icon-name", Config.APP_ID,
                               "version", Config.VERSION,
                               "comments", "Calculator and random generator password for GNOME.",
                               "copyright", COPYRIGHT,
                               "authors", AUTHORS,
                               "license-type", Gtk.License.LGPL_3_0,
                               "wrap-license", false,
                               "website", "https://gitlab.com/elescoute/password-for-gnome-vala",
                               "translator-credits", _("translator-credits"),
                               null);
        }

        void quitter()
        {
            //close();
            if(!attendreAvanDeFermer) get_application().quit();
            if(attendreAvanDeFermer) quitterAppli=true;
        }

        void effacement()
        {
            clipboard("");

            if(switchNotifications.get_state()) envoiNotification(_("Password deleted"));//SI FENETRE HIDE, PROGRAMME S'ARRÊTE TOUT DE SUITE (???)

            attendreAvanDeFermer=false;
            if(quitterAppli) Timeout.add(1000, () => {quitter(); return false;});
        }

        void nettoyer()
        {
            iconeBoutonNettoyer.set_from_icon_name("user-trash-symbolic", BUTTON);
            clipboard("");
            if(texteSecret.get_text()=="") texteAlias.set_text("");
            texteSecret.set_text("");
            passwordCourt="";
            randomPassword="";
            motDePasse.set_label(_("your password"));
            motDePasseAleatoire.set_label("");
            labelFaille.set_label("");
            labelFailleAleatoire.set_label("");
            revealCopier.set_reveal_child(false);
        }

        void copier()
        {
            if(fenetre.get_visible_child_name()=="pageCalculateur" && passwordCourt!="") clipboard(passwordCourt);
            if(fenetre.get_visible_child_name()=="pageGenerateur" && pass!="") clipboard(pass);

            if(switchNotifications.get_state()) envoiNotification(_("Password copied to the clipboard"));

            if(switchSuppression.get_state()){
                if(!quitterAppli) attendreAvanDeFermer=true;//AU CAS OU L'UTILISATEUR VEUILLE FERMER L'APPLI APRES AVOIR APPUYER SUR COPIE
                Timeout.add((uint) scaleTemps.get_value()*1000, () => {effacement(); return false;});
            }

            if(quitterAppli) hide();
        }

        void entreTexteSecret()
        {

            if(passwordCourt!=""){
                if(switchQuitter.get_state()){
                    quitterAppli=true;
                    attendreAvanDeFermer=true;
                }

                //COPIE MOT DE PASSE
                copier();
            }
        }

        void chgtTaille(Gtk.Allocation allocation)
        {
            int width;
            int height;
            get_size(out width, out height);
            if(width!=largeur){
                largeur=width;
                if(largeur<=480 && !phosh){
                    phosh=true;
                    if(fenetre.get_visible_child_name()=="pageCalculateur" || fenetre.get_visible_child_name()=="pageGenerateur") revealBarre.set_reveal_child(true);
                    revealHaut.set_reveal_child(false);
                    revealHaut.remove(barre);
                    revealBas.add(barre);
                    revealBas.set_reveal_child(true);
                    //headerbar.remove(boutonMenuMode);
                    //headerbar.pack_start(boutonMenuMode);
                }
                if(largeur>480 && phosh){
                    phosh=false;
                    revealBarre.set_reveal_child(false);
                    revealBas.set_reveal_child(false);
                    revealBas.remove(barre);
                    revealHaut.add(barre);
                    if(fenetre.get_visible_child_name()=="pageCalculateur" || fenetre.get_visible_child_name()=="pageGenerateur") revealHaut.set_reveal_child(true);
                    //headerbar.remove(boutonMenuMode);
                    //headerbar.pack_end(boutonMenuMode);
                }
            }
        }

        void envoiNotification(string id)
        {
            var notification = new Notification ("Password");
            notification.set_body (id);
            notification.set_icon (new ThemedIcon("dialog-password"));
            Application.get_default ().send_notification (null, notification);
        }

        void clipboard(string id)
        {
            Gdk.Display display = Gdk.Display.get_default ();
            Gtk.Clipboard refClipboard = Gtk.Clipboard.get_for_display (display, Gdk.SELECTION_CLIPBOARD);
            refClipboard.set_text(id, id.length);
        }

        void changeTheme(bool ecriture)
        {
            Gtk.Settings.get_default().set("gtk-application-prefer-dark-theme", modeSombre.get_active());
            if(ecriture){
                var dis = FileStream.open (dossierConfig + "/theme.conf", "w");
                dis.write(modeSombre.get_active().to_string().data);
            }
            //MISE À JOUR ICONE MONITOR ET PWNED EN FONCTION DU THÈME
            if(modeSombre.get_active()){
                iconePwn.set_from_icon_name("iconePwned", MENU);
                iconeMonitor.set_from_icon_name("firefox_monitor", MENU);
                if(fenetre.get_visible_child_name()=="pageMonitor") iconeBoutonMenuMode.set_from_icon_name("firefox_monitor", BUTTON);
                if(fenetre.get_visible_child_name()=="pagePwned") iconeBoutonMenuMode.set_from_icon_name("iconePwned", BUTTON);
            }
            else{
                iconePwn.set_from_icon_name("iconePwned_light", MENU);
                iconeMonitor.set_from_icon_name("firefox_monitor_light", MENU);
                if(fenetre.get_visible_child_name()=="pageMonitor") iconeBoutonMenuMode.set_from_icon_name("firefox_monitor_light", BUTTON);
                if(fenetre.get_visible_child_name()=="pagePwned") iconeBoutonMenuMode.set_from_icon_name("iconePwned_light", BUTTON);
            }
        }

        //PROFILS
        void majListeProfils()
        {

            //NETTOYAGE LISTE
            listeProfils.foreach ((entry) => listeProfils.remove (entry));
            //NETTOYAGE MENU
            boxMenuProfil.foreach ((element) => {if(element!=boutonProfilRetour) boxMenuProfil.remove (element);});
            //NETTOYAGE BOUTONS
            boutonChargProfil.foreach ((element) => boutonChargProfil.remove (element));

            //ESSAI



            //RECHERCHE DES PROFILS
            try{
                Dir dir = Dir.open (dossierConfig);
                string? name = null;
                while((name = dir.read_name()) != null){
                    if(name.substring(0,9)=="password-" && name.substring(name.char_count()-5,5)==".conf"){
                        listeProfils.append(name.substring(9,name.char_count()-14));
                    }
                }
            } catch (FileError err){}



            nbProfils =(int) listeProfils.length();

            if(nbProfils > 0) boutonProfil.show();
            if(nbProfils == 0) boutonProfil.hide();

            //CRÉATION BOUTONS

            for(int i=0;i<nbProfils;i++){
                int j = i;
                boutonChargProfil.append(new Gtk.ModelButton());
                boutonChargProfil.nth_data((uint) i).set_label(listeProfils.nth_data((uint) i));
                boutonChargProfil.nth_data((uint) i).set_halign(FILL);
                boutonChargProfil.nth_data((uint) i).set_margin_start(10);
                boutonChargProfil.nth_data((uint) i).set_margin_end(10);
                boutonChargProfil.nth_data((uint) i).set_margin_top(4);
                boutonChargProfil.nth_data((uint) i).set_margin_bottom(4);
                boxMenuProfil.pack_start(boutonChargProfil.nth_data((uint) i));
                boutonChargProfil.nth_data((uint) i).show();
                boutonChargProfil.nth_data((uint) i).clicked.connect(() => {string profil = listeProfils.nth_data(j);chargementProfil(profil);});
            }
        }

        void chargementProfil(string profil)
        {
            headerbar.set_subtitle(_("Profile: ") + profil);
            profilActuel = profil;
            if(codeVerrouillage == "") lectureFichierConfig();
            if(codeVerrouillage != "") lectureFichierCryptConfig();
            //AFFICHAGE DU BOUTON SUPPRIMER PROFIL SI ON EST SUR LA PAGE RÉGLAGES
            if(fenetre.get_visible_child_name()=="pageReglages" && !boutonSupprimerProfil.is_visible()) boutonSupprimerProfil.show();
            //ÉCRITURE FICHIER DERNIER PROFIL CHARGÉ
            var dis = FileStream.open (dossierConfig + "/profile.conf", "w");
            dis.write(profil.data);
        }

        void creationProfil()
        {
            Gtk.MessageDialog nouveauProfil = new Gtk.MessageDialog(this, Gtk.DialogFlags.MODAL, Gtk.MessageType.WARNING,Gtk.ButtonsType.NONE ,_("Enter a name for the new profile:"));
            Gtk.Widget annuler;
            annuler = nouveauProfil.add_button(_("Cancel"),Gtk.ResponseType.REJECT);
            Gtk.Widget creer;
            creer = nouveauProfil.add_button(_("Create"),Gtk.ResponseType.ACCEPT);
            creer.get_style_context().add_class(Gtk.STYLE_CLASS_SUGGESTED_ACTION);
            //confirmation.add_buttons(_("test1"),Gtk.ResponseType.REJECT,_("test2"),Gtk.ResponseType.ACCEPT);
            Gtk.Entry nom = new Gtk.Entry();
            nom.set_margin_start(20);
            nom.set_margin_end(20);
            Gtk.Box dialogBox = nouveauProfil.get_content_area();
            dialogBox.pack_end(nom);
            nouveauProfil.show_all();

            Gtk.ResponseType result = (Gtk.ResponseType)nouveauProfil.run ();
            string nomProfil = nom.get_text();
            nouveauProfil.close();
            if (result == Gtk.ResponseType.ACCEPT && nomProfil!=""){
                profilActuel = nomProfil;
                if(codeVerrouillage == "") ecritureConfig(profilActuel);
                if(codeVerrouillage != "") ecritureCryptConfig(profilActuel);
                majListeProfils();
                headerbar.set_subtitle(_("Profile: ") + profilActuel);
                //ÉCRITURE FICHIER DERNIER PROFIL CHARGÉ
                var disProfil = FileStream.open (dossierConfig + "/profile.conf", "w");
                disProfil.write(profilActuel.data);
            }
        }

        void suppressionProfil()
        {
            //AVERTISSEMENT
            Gtk.MessageDialog confirmation = new Gtk.MessageDialog(this, Gtk.DialogFlags.MODAL, Gtk.MessageType.WARNING,Gtk.ButtonsType.NONE ,_("Are you sure you want to delete the profile ")+profilActuel+"?");
            Gtk.Widget annuler;
            annuler = confirmation.add_button(_("Cancel"),Gtk.ResponseType.REJECT);
            Gtk.Widget effacer;
            effacer = confirmation.add_button(_("Delete"),Gtk.ResponseType.ACCEPT);
            effacer.get_style_context().add_class(Gtk.STYLE_CLASS_DESTRUCTIVE_ACTION);
            //confirmation.add_buttons(_("test1"),Gtk.ResponseType.REJECT,_("test2"),Gtk.ResponseType.ACCEPT);
            Gtk.ResponseType result = (Gtk.ResponseType)confirmation.run ();
            confirmation.close();

            if (result == Gtk.ResponseType.ACCEPT){
                FileUtils.remove(dossierConfig + "/password-" + profilActuel + ".conf");
                majListeProfils();
                //ON CHARGE LE PREMIER PROFIL DE LA LISTE SI LISTE NON VIDE
                if(listeProfils.length()==0) profilActuel = "";
                if(listeProfils.length() >0) chargementProfil(listeProfils.nth_data(0));
                basculModeReglages();
                //SUPPRESSION FICHIER DERNIER PROFIL CHARGÉ
                FileUtils.remove (dossierConfig + "/profile.conf");
            }
        }

        void chiffrementProfils()
        {
            for(int i=0;i<listeProfils.length();i++){
                if(listeProfils.nth_data((uint) i) != profilActuel){
                    string profile = listeProfils.nth_data((uint) i);
                    FileUtils.rename(dossierConfig + "/password-" + profile +".conf", dossierConfig + "/tmp-password.conf");
                    string fichierTemp = dossierConfig + "/tmp-password.conf";
                    var dis = FileStream.open (fichierTemp, "r");
                    string config = "";
                    string cle = mdpCalcul(codeVerrouillage,codeVerrouillage, true, 4, true, 42 ,"", false , false , false , false , true, true);
                    config += chiffrement(dis.read_line(),cle.slice(0,2),1,false) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(2,4),1,false) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(2,3),1,false) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(3,4),1,true) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(5,6),1,false) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(6,7),1,true) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(7,8),1,true) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(8,9),1,true) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(9,10),1,true) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(13,14),1,true) + "\n";
                    var disProfil = FileStream.open (dossierConfig + "/password-" + profile +".conf", "w");
                    disProfil.write(config.data);
                    FileUtils.remove(dossierConfig + "/tmp-password.conf");
                }
            }
        }

        void dechiffrementProfils()
        {
            for(int i=0;i<listeProfils.length();i++){
                if(listeProfils.nth_data((uint) i) != profilActuel){
                    string profile = listeProfils.nth_data((uint) i);
                    FileUtils.rename(dossierConfig + "/password-" + profile +".conf", dossierConfig + "/tmp-password.conf");
                    string fichierTemp = dossierConfig + "/tmp-password.conf";
                    var dis = FileStream.open (fichierTemp, "r");
                    string config = "";
                    string cle = mdpCalcul(codeVerrouillage,codeVerrouillage, true, 4, true, 42 ,"", false , false , false , false , true, true);
                    config += chiffrement(dis.read_line(),cle.slice(0,2),-1,false) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(2,4),-1,false) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(2,3),-1,false) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(3,4),-1,true) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(5,6),-1,false) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(6,7),-1,true) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(7,8),-1,true) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(8,9),-1,true) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(9,10),-1,true) + "\n";
                    config += chiffrement(dis.read_line(),cle.slice(13,14),-1,true) + "\n";
                    var disProfil = FileStream.open (dossierConfig + "/password-" + profile +".conf", "w");
                    disProfil.write(config.data);
                    FileUtils.remove(dossierConfig + "/tmp-password.conf");
                }
            }
        }

	}   //FIN WINDOW
}   //FIN NAMESPACE
