/*int checkPwned(string mdpInit){

    int faille = 0;

    string sha1mdp = GLib.Checksum.compute_for_string (ChecksumType.SHA1, mdpInit);
    string url = "https://api.pwnedpasswords.com/range/"+sha1mdp.substring(0,5);
    var session = new Soup.Session ();
    var message = new Soup.Message ("GET", url);
    session.send_message (message);
    string chaine = (string) message.response_body.data;
    string[] liste = chaine.split("\n");
    string[] mdp = {""};
    string[] occ = {""};
    for(int i=0;i<liste.length;i++){
        string[] temp = liste[i].split(":");
        mdp += temp[0];
        occ += temp[1];
    }
    for(int i=1;i<mdp.length;i++){
        if(mdp[i] == sha1mdp.substring(5).ascii_up()) faille = int.parse(occ[i]);
    }

    return faille;
}*/

int appelCheckPwned(string mdpInit){
    int faille = 0;
    var loop = new MainLoop();
	checkPwned.begin(mdpInit, (obj,res) => {
	    try{faille = checkPwned.end(res);}
	    catch(ThreadError e){}
	    loop.quit();
	});
	loop.run();
	return faille;
}

async int checkPwned(string mdpInit) throws ThreadError{

    SourceFunc callback = checkPwned.callback;

    int faille = 0;

    ThreadFunc<bool> run = () => {
        string sha1mdp = GLib.Checksum.compute_for_string (ChecksumType.SHA1, mdpInit);
        string url = "https://api.pwnedpasswords.com/range/"+sha1mdp.substring(0,5);
        var session = new Soup.Session ();
        var message = new Soup.Message ("GET", url);
        session.send_message (message);
        string chaine = (string) message.response_body.data;
        int longueurMessage = (int) message.response_body.length;

        if(longueurMessage!=0){
            bool stop = false;

            for(int i=0;i<chaine.length;i++){
                if(chaine[i]==':' && !stop){
                    if(chaine.substring(i-35,35)==sha1mdp.substring(5).ascii_up()){
                        string[] temp = chaine.substring(i+1).split("\n");
                        faille = int.parse(temp[0]);
                        stop = true;
                    }
                }
            }
        }
        else{
            //PROBLÈME CONNEXION
            faille = -1;
        }
        Idle.add((owned) callback);
        return true;
    };
    new Thread<bool>("checkPwned",run);
    yield;
    return faille;
}

